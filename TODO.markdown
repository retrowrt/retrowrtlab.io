---
layout: page
title: TODO 
permalink: /TODO/
---
Important things:
* Package X.org or XFree86, make it build with musl libc. Since X.org has dropped a lot of legacy graphics drivers, XFree86 might be preferable, also related to RAM usage.
* Package TinyX from TinyCoreLinux
* Fix booting banner
* Make install/configuration shell script that does not use significant amount of RAM (for password, hostname, etc)
* Add swap partition. Since Linux does overcommitting, this makes it possible to at least utilize the full RAM
* Use syslinux instead of grub
* Add tpdualscan
* Include this patch: https://gitlab.com/eloydegen/mettler-toledo-linux/-/blob/master/i686-emu.patch

Possible ideas:
* Check which binaries use unsupported opcodes: [elfx86exts](https://github.com/pkgw/elfx86exts)
* Port Opemu to SSE2/SSE/MMX: https://github.com/mirh/opemu-linux
* Package [NCSA Mosaic](https://github.com/alandipert/ncsa-mosaic)
* Package manpages. OpenWrt does not have them because it's not included by Buildroot which does not target user facing Linux machines