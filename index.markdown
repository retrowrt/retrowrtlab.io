---
layout: page 
title: Home
---
![RetroWrt logo](/assets/RetroWrt_logo_small.png){: width="250" }

RetroWrt is an OpenWrt based Linux distribution for 32-bit x86 hardware from the Intel 80486 and upwards.

Currently this project is in pre-alpha state, feel free to talk in [#retrowrt on Libera.chat](https://web.libera.chat/#retrowrt).

![Running on a 365XD](/images/thinkpad_365XD.jpg)