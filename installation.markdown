---
layout: page
title: Installation 
permalink: /installation/
---
Use a CF->IDE adapter to replace the original IDE disk for faster flash, or write the image to the disk, possibly with a USB->IDE adapter:

`dd if=image.img of=/dev/sdX bs=4M status=progress conv=sync`

Where `image.img` is the name of the RetroWrt file and `sdX` is the name of the disk.
