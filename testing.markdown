---
layout: page
title: Testing 
permalink: /testing/
---
QEMU can virtualized older versions of x86 CPUs: https://qemu.readthedocs.io/en/latest/system/qemu-cpu-models.html#other-non-recommended-x86-cpus

They are non-recommended, but we need them to make sure RetroWrt runs on our targets.

Our goal is to have as much workong on i486 as possible, but when that is not feasable due to inline assembly, we can increase the target to Pentium or Pentium II. Hopefully not higher than that :)

# Installation instructions
First download the disk image from the [downloads](./download) page.
## Debian

## Fedora