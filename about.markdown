---
layout: page
title: About
permalink: /about/
---

RetroWrt is based on OpenWrt.

The RetroWrt project is maintained by [Eloy Degen](https://eloydegen.com).

All open source work is licensed under the respective license of those projects. My own work is published under the CC0, therefore in the public domain. 
