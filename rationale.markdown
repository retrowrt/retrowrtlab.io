---
layout: page
title: Rationale 
permalink: /rationale/
---
Don't we have enough Linux distros already? That's what I've always thought as well, but for this specific use case, there wasn't a suitable option.

Most Linux distributions have gradually increased the requirements for 32-bit x86 with various ISA extensions such as SSE2, but the Linux kernel and software like musl and glibc are still supporting hardware as old as the 486.

The only mainstream Linux distributions that have reasonable support for 486-class hardware are Gentoo, Slackware. But Gentoo requires compiling everything from source and Slackware requires 64MiB of RAM. The latter might be suitable for your machine, but surely not every machine from that era has that amount of RAM. For example, an IBM ThinkPad [365ED](https://en.wikipedia.org/wiki/IBM_ThinkPad_365) is limited to 24MiB of RAM. OpenWrt is tailored at very constrained embedded systems, and in terms of resources, a modern embedded system is comparable to a workstation from the 90s. The default OpenWrt x86 legacy build is wasting some RAM because it is built for networking appliances, therefore includes code that would not be needed for a workstation. Some custom-built disk images for legacy x86 devices have been developed over time, but that makes it hard to install packages, because they are not a distribution.

OpenWrt [bumped](https://lists.infradead.org/pipermail/openwrt-devel/2020-January/026892.html) their system requirements to the Pentium MMX upwards from the 486 in 2020, due to ffmpeg being broken. This project reverts that change. We don't support ffmpeg either, but lot's of other packages don't require more modern assembly.

The reason is that ffmpeg's x86


Note: support of the kernel, libc or even distro as a whole does not guarantees that any other software works at all. Software can use inline assembly, which will be included regardless of what target is given to the compiler. This includes the CMOV opcode for example, which is commonly used in JITs in software like WebKit.

The X.org server and the Linux graphics stack in Linux is known for dropping support for old GPUs. This is an attempt to fix some of this by packaing software that includes older drivers.

RetroWrt is available in the form of disk images and source code instead of installer ISOs. This is because installation generally takes some RAM, which might be more than the machine has available.

<center><b>Minimum system requirements of various Unix-like distros</b></center>

| Distro | x86 ISA | RAM | Ref |
|---|---|---|---|
| Debian | i686, no SSE or MMX | 780MB | [Ref1](https://wiki.debian.org/ArchitectureSpecificsMemo#Architecture_baselines) [Ref2](https://www.debian.org/releases/bullseye/amd64/ch02s05.en.html) |
| Alpine | i686 with SSE2 | 100MB | [Ref](https://wiki.alpinelinux.org/wiki/Installation#Minimal_Hardware_Requirements) |
| Fedora | x64 required | 2GiB | [Ref](https://fedoraproject.org/wiki/Changes/Stop_Building_i686_Kernels) |
| Ubuntu | No ISOs available for 32 bit x86, being phased out | 4GiB | [Ref](https://ubuntu.com/download/desktop) |
| Gentoo | i486 | 265MiB | [Ref](https://wiki.gentoo.org/wiki/Handbook:X86/Full/Installation#Hardware_requirements) |
| Tiny Core Linux | i486DX | 28MB | [Ref](http://tinycorelinux.net/faq.html#req) |
| Slackware | i486 | 64MiB | [Ref](http://www.slackware.com/install/sysreq.php) |
| Arch Linux 32 | i486 | 256MiB | RAM according to IRC user |
| Minix | i586 | 32MiB | [Ref](https://wiki.minix3.org/doku.php?id=usersguide:hardwarerequirements) |

# What if I have an i386 or older CPU?
i386 should not to be confused with "i386", the name that distros use when they actually mean i686, possibly with more required exensions. In December 2012, Linus Torvalds merged [a patch](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=743aa456c1834f76982af44e8b71d1a0b2a82e21) by Ingo Molnár which dropped support for the i386 in the Linux kernel.

For i386 support: check out [ELKS](https://github.com/jbruchon/elks), the Embeddable Linux Kernel Subset! It's a fork of Linux that was started in the 90s, which is being actively developed again since 2012 after being abandoned for some years. It's targeted at 16-bit systems, but works fine on the 32-bit 386 in 16-bit mode.