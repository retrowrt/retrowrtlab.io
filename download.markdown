---
layout: page 
title: Download
permalink: /download/
---
* First alpha build: [download here](https://eloydegen.com/openwrt-x86-legacy-generic-squashfs-combined.img)
* RetroWrt source code [download here](https://gitlab.com/retrowrt/retrowrt)
* Website source code [download here](https://gitlab.com/retrowrt/retrowrt.gitlab.io)
