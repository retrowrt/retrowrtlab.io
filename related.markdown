---
layout: page 
title: Related projects 
permalink: /related/
---
# Recently (2015–current) updated projects
* [Tiny Core Linux](http://tinycorelinux.net/)
* [Embeddable Linux Kernel Subset](https://github.com/jbruchon/elks)
* [Gentoo on a 486](https://github.com/yeokm1/gentoo-on-486)
* [Linux on a 486SX](https://ocawesome101.github.io/486-linux.html) + [Git repo](https://github.com/Ocawesome101/486linux)
* [Linux kernel tinification project](https://tiny.wiki.kernel.org/) and [presentation](http://events17.linuxfoundation.org/sites/events/files/slides/Linux_In_a_Lightbulb-Where_are_we_on_tinification-ELCE2015-final_0.pdf) including including the history of Linux kernel tinification
## Graphics
* [X11 Conservancy Project](https://x11cp.org/)
* [The Nano-X Window System](http://www.microwindows.org/)
* [KDE Restoration Project](https://en.wikipedia.org/wiki/K_Desktop_Environment_1#KDE_Restoration_Project)

# Older projects and various links
* [Linux on the Toshiba T1910](https://michaelminn.com/linux/toshiba1910/)
* [Booting Linux on an old i586 Pentium MMX](https://dflund.se/~triad/krad/linux-on-pentium-mmx.html)
* [Which Linux or BSD distributions do still support i386, i486 or i586 CPUs?](https://retrocomputing.stackexchange.com/questions/1811/which-linux-or-bsd-distributions-do-still-support-i386-i486-or-i586-cpus)